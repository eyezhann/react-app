import React, { Component } from "react";
import { connect } from "react-redux";
import { addToCart } from "../../../store/actions/cartActions";

import "../../shared/header/Header-shop.scss";
import "./Shop.scss";

class Shop extends Component {
  handleClick = id => {
    this.props.addToCart(id);
  };

  render() {
    let itemList = this.props.items.map(item => {
      return (
        <div className="Product__column" key={item.id}>
          <img src={item.img} alt={item.title} className="Product__img" />
          <div className="Product__description">
            <p className="Product__title">{item.title}</p>
            <p className="Product__price">${item.price}</p>
            <button
              className="to-cart"
              onClick={() => {
                this.handleClick(item.id);
              }}
            >
              add to cart
            </button>
          </div>
        </div>
      );
    });

    return (
      <div>
        <div className="Product__row">{itemList}</div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: id => {
      dispatch(addToCart(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shop);
