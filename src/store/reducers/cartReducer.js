import Item1 from "../../images/set-top.jpg";
import Item2 from "../../images/sunscreen-top.jpg";
import Item3 from "../../images/wipes-top.jpg";
import Item4 from "../../images/body-bundle.jpg";
import Item5 from "../../images/makeup-bag.jpg";
import Item6 from "../../images/toner.jpg";
import Item7 from "../../images/summer-scrub.jpg";
import Item8 from "../../images/walnut-scrub.jpg";
import Item9 from "../../images/serum.jpg";

import {
  ADD_TO_CART,
  REMOVE_ITEM,
  ADD_QUANTITY,
  SUB_QUANTITY
} from "../actions/action-types/cart-actions";

const initState = {
  items: [
    {
      id: 1,
      title: "kylie skin set",
      price: 125,
      img: Item1
    },
    {
      id: 2,
      title: "broad spectrum spf 30 sunscreen oil",
      price: 32,
      img: Item2
    },
    {
      id: 3,
      title: "makeup removing wipes",
      price: 10,
      img: Item3
    },
    {
      id: 4,
      title: "summer body bundle",
      price: 22,
      img: Item4
    },
    {
      id: 5,
      title: "kylie skin travel bag",
      price: 82,
      img: Item5
    },
    {
      id: 6,
      title: "vanilla milk toner",
      price: 50,
      img: Item6
    },
    {
      id: 7,
      title: "coconut body scrub",
      price: 26,
      img: Item7
    },
    {
      id: 8,
      title: "walnut face scrub",
      price: 28,
      img: Item8
    },
    {
      id: 9,
      title: "vitamin c serum",
      price: 36,
      img: Item9
    }
  ],
  addedItems: [],
  total: 0
};

const cartReducer = (state = initState, action) => {
  if (action.type === ADD_TO_CART) {
    let addedItem = state.items.find(item => item.id === action.id);

    let existed_item = state.addedItems.find(item => action.id === item.id);
    if (existed_item) {
      addedItem.quantity += 1;
      return {
        ...state,
        total: state.total + addedItem.price
      };
    } else {
      addedItem.quantity = 1;

      let newTotal = state.total + addedItem.price;

      return {
        ...state,
        addedItems: [...state.addedItems, addedItem],
        total: newTotal
      };
    }
  }
  if (action.type === REMOVE_ITEM) {
    let itemToRemove = state.addedItems.find(item => action.id === item.id);
    let new_items = state.addedItems.filter(item => action.id !== item.id);

    let newTotal = state.total - itemToRemove.price * itemToRemove.quantity;
    console.log(itemToRemove);
    return {
      ...state,
      addedItems: new_items,
      total: newTotal
    };
  }

  if (action.type === ADD_QUANTITY) {
    let addedItem = state.items.find(item => item.id === action.id);
    addedItem.quantity += 1;
    let newTotal = state.total + addedItem.price;
    return {
      ...state,
      total: newTotal
    };
  }
  if (action.type === SUB_QUANTITY) {
    let addedItem = state.items.find(item => item.id === action.id);

    if (addedItem.quantity === 1) {
      let new_items = state.addedItems.filter(item => item.id !== action.id);
      let newTotal = state.total - addedItem.price;
      return {
        ...state,
        addedItems: new_items,
        total: newTotal
      };
    } else {
      addedItem.quantity -= 1;
      let newTotal = state.total - addedItem.price;
      return {
        ...state,
        total: newTotal
      };
    }
  } else {
    return state;
  }
};
export default cartReducer;
